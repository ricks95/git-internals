package core;

import java.io.*;
import java.util.zip.*;

public class GitBlobObject {
    private String path;
    private String hash;

    public GitBlobObject (String repoPath, String objHash) {
        path = repoPath;
        hash = objHash;
    }

    public String getType () {
        StringBuilder sb = getSB ();
        int space = sb.indexOf (" ");
        return sb.substring (0, space);
    }

    public String getContent () {
        StringBuilder sb = getSB ();
        int space = sb.indexOf ("\u0000");
        sb = sb.delete (0, space + 1);
        return sb.toString ();
    }

    private StringBuilder getSB () {
        StringBuilder sb = new StringBuilder ();
        int readChar = 0;
        String file = path + "/objects/" + hash.substring (0, 2) + "/" + hash.substring (2);

        try {
            FileInputStream is = new FileInputStream (file);
            InflaterInputStream inflater = new InflaterInputStream (is);
            while ((readChar = inflater.read ()) >= 0) {
                sb.append ((char) readChar);
            }
        }
        catch (IOException e) {
            e.printStackTrace ();
        }
        return sb;
    }
}
