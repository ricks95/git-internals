package core;

import java.io.*;

public class GitRepository {
    private String path;

    public GitRepository (String repo) {
        path = repo;
    }

    public String getHeadRef () {
        File head = new File (path + "/HEAD");
        String returned = "";

        try {
            BufferedReader br = new BufferedReader (new FileReader (head));
            br.skip (5);
            returned = br.readLine ();
        }
        catch (IOException e) {
            e.printStackTrace ();
        }

        return returned;
    }

    public String getRefHash (String ref) {
        File hash = new File (path + "/" + ref);
        String commitHash = "";

        try {
            BufferedReader br = new BufferedReader (new FileReader (hash));
            commitHash = br.readLine ();
        }
        catch (IOException e) {
            e.printStackTrace ();
        }
        return commitHash;
    }
}
